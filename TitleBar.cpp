#include "TitleBar.h"
#include <QMouseEvent>

TitleBar::TitleBar( QWidget *parent ) : QFrame( parent ) {
	setupUi( this );
	getWindow()->setWindowFlags( Qt::FramelessWindowHint | Qt::CustomizeWindowHint );
	btnClose->setStyleSheet( "border-radius: 5px;" );
	btnMinimize->setStyleSheet( "border-radius: 5px;" );
	btnAbove->setStyleSheet( "border-radius: 5px;" );
}

QString TitleBar::windowTitle() {
	return wndTitle->text();
}

void TitleBar::setWindowTitle( const QString &title ) {
	wndTitle->setText( title );
}

QFrame *TitleBar::getWindow( QObject *obj ) {
	if ( !obj ) obj = this;
	if ( obj->parent() == nullptr ) return reinterpret_cast<QFrame *>( obj );
	return getWindow( obj->parent() );
}

void TitleBar::changeEvent( QEvent *e ) {
	QFrame::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void TitleBar::mousePressEvent( QMouseEvent *event ) {
	if ( event->button() == Qt::LeftButton ) {
		cursor = event->globalPos() - getWindow()->geometry().topLeft();
		setCursor( QCursor( Qt::ClosedHandCursor ) );
		event->accept();
	}
	return QFrame::mousePressEvent( event );
}

void TitleBar::mouseReleaseEvent( QMouseEvent *event ) {
	if ( event->button() == Qt::LeftButton ) {
		setCursor( QCursor( Qt::OpenHandCursor ) );
		event->accept();
	}
	return QFrame::mousePressEvent( event );
}

void TitleBar::mouseMoveEvent( QMouseEvent *event ) {
	if ( event->buttons() & Qt::LeftButton ) {
		getWindow()->move( event->globalPos() - cursor );
		event->accept();
	}
	return QFrame::mouseMoveEvent( event );
}

void TitleBar::on_btnAbove_toggled( bool checked ) {
	emit onToggleStayOnTop( checked );
	getWindow()->setWindowFlag( Qt::WindowStaysOnTopHint, checked );
	getWindow()->show();
}

void TitleBar::on_btnMinimize_clicked() {
	emit onMinimize();
	getWindow()->showMinimized();
}

void TitleBar::on_btnClose_clicked() {
	emit onClose();
	getWindow()->close();
}
