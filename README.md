# TitleBar

![Screenshot (dark system theme)](Screenshot.png)
![Screenshot (light system theme)](Screenshot2.png)

### Buttons:

* Close
* Minimize
* Stay on top

### Behavior

* Pointing hand cursor on buttons
* open/closed hand cursor on title

## Note

Window movable, but not resizable!