#ifndef TITLEBAR_H
#define TITLEBAR_H

#include "ui_TitleBar.h"

class TitleBar : public QFrame, private Ui::TitleBar {
	Q_OBJECT

public:
	explicit TitleBar( QWidget *parent = nullptr );

	QString windowTitle();
	void	setWindowTitle( const QString &title );

protected:
	QFrame *getWindow( QObject *obj = nullptr );
	void changeEvent( QEvent *e );
	void mousePressEvent( QMouseEvent *event );
	void mouseReleaseEvent( QMouseEvent *event );
	void mouseMoveEvent( QMouseEvent *event );

	QPoint cursor;

private slots:
	void on_btnAbove_toggled( bool checked );
	void on_btnMinimize_clicked();

	void on_btnClose_clicked();

signals:
	void onToggleStayOnTop( bool );
	void onMinimize();
	void onClose();
};

#endif // TITLEBAR_H
